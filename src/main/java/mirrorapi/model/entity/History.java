package mirrorapi.model.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;

@Entity
@Table(name = "\"DEALS_HISTORY\"")
public class History {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "\"Id\"")
    private Long id;

    @Column(name = "\"DealId\"")
    private Long dealId;

    @Column(name = "\"Version\"", length = 100)
    private String version;

    @Column(name = "\"Status\"", length = 32)
    private String status;

    @Column(name = "\"Remark\"", length = 1000)
    private String remark;

    @Column(name = "\"ActorId\"", length = 100)
    private String actorId;

    @Column(name = "\"DataUpdate\"", length = 2)
    private String dataUpdate;

    @Column(name = "\"InsertTimestamp\"")
    @CreationTimestamp
    private java.sql.Timestamp insertTimestamp;

    public History(Deal deal){
        this.dealId = deal.getId();
        this.status = deal.getStatus();
        this.remark = deal.getRemark();
        this.actorId = "";
        this.dataUpdate = "A";
        this.version = deal.getVersion();
    }

    public History() {}

    @Override
    public String toString(){
        ObjectMapper mapper = new ObjectMapper();

        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDealId(Long dealId) {
        this.dealId = dealId;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return this.remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getActorId() {
        return this.actorId;
    }

    public void setActorId(String actorId) {
        this.actorId = actorId;
    }

    public void setDataUpdate(String dataUpdate) {
        this.dataUpdate = dataUpdate;
    }

    public java.sql.Timestamp getInsertTimestamp() {
        return this.insertTimestamp;
    }

    public void setInsertTimestamp(java.sql.Timestamp insertTimestamp) {
        this.insertTimestamp = insertTimestamp;
    }
}

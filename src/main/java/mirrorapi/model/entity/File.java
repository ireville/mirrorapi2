package mirrorapi.model.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;

@Entity
@Table(name = "\"DEALS_FILES\"")
public class File {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "\"Id\"")
    private Long id;

    @Column(name = "\"DealId\"")
    private Long dealId;

    @Column(name = "\"Parent\"")
    private Long parent;

    @Column(name = "\"Relation\"", length = 32)
    private String relation;

    @Column(name = "\"Kind\"", length = 32)
    private String kind;

    @Column(name = "\"Status\"", length = 32)
    private String status;

    @Column(name = "\"LocalPath\"", length = 256)
    private String localPath;

    @Column(name = "\"DfsPath\"", length = 100)
    private String dfsPath;

    @Column(name = "\"Hash\"", length = 100)
    private String hash;

    @Column(name = "\"Sign\"", length = 100)
    private String sign;

    @Column(name = "\"Error\"", length = 256)
    private String error;

    @Column(name = "\"DataUpdate\"", length = 2)
    private String dataUpdate;

    @Column(name = "\"Version\"", length = 100)
    private String version;

    @Column(name = "\"Remark\"", length = 16000)
    private String remark;

    @Column(name = "\"Recipients\"", length = 16000)
    private String recipients;

    @Column(name = "\"FileUUID\"", length = 64)
    private String fileUuid;

    @Column(name = "\"InsertTimestamp\"")
    @CreationTimestamp
    private java.sql.Timestamp insertTimestamp;

    @Column(name = "\"FromSC\"", length = 16)
    private String fromSc;

    public File() {}

    public File(String kind, String status, String remark, String fileUuid){
        this.kind = kind;
        this.status = status;
        this.remark = remark;
        this.fileUuid = fileUuid;
    }

    @Override
    public String toString(){
        ObjectMapper mapper = new ObjectMapper();

        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDealId() {
        return this.dealId;
    }

    public void setDealId(Long dealId) {
        this.dealId = dealId;
    }

    public Long getParent() {
        return this.parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    public String getRelation() {
        return this.relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getKind() {
        return this.kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLocalPath() {
        return this.localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public String getDfsPath() {
        return this.dfsPath;
    }

    public void setDfsPath(String dfsPath) {
        this.dfsPath = dfsPath;
    }

    public String getHash() {
        return this.hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getSign() {
        return this.sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getError() {
        return this.error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getDataUpdate() {
        return this.dataUpdate;
    }

    public void setDataUpdate(String dataUpdate) {
        this.dataUpdate = dataUpdate;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getRemark() {
        return this.remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRecipients() {
        return this.recipients;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    public String getFileUuid() {
        return this.fileUuid;
    }

    public void setFileUuid(String fileUuid) {
        this.fileUuid = fileUuid;
    }

    public java.sql.Timestamp getInsertTimestamp() {
        return this.insertTimestamp;
    }

    public void setInsertTimestamp(java.sql.Timestamp insertTimestamp) {
        this.insertTimestamp = insertTimestamp;
    }

    public String getFromSc() {
        return this.fromSc;
    }

    public void setFromSc(String fromSc) {
        this.fromSc = fromSc;
    }
}

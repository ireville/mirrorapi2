package mirrorapi.model.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;

@Entity
@Table(name = "\"DEALS_STATUSMAP\"")
public class StatusMap {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "\"Id\"")
    private Long id;

    @Column(name = "\"DealId\"")
    private Long dealId;

    @Column(name = "\"Status\"", length = 32)
    private String status;

    @Column(name = "\"StatusNext\"", length = 32)
    private String statusNext;

    @Column(name = "\"Role\"", length = 32)
    private String role;

    @Column(name = "\"InsertTimestamp\"")
    @CreationTimestamp
    private java.sql.Timestamp insertTimestamp;

    public void setId(Long id) {
        this.id = id;
    }

    public void setDealId(Long dealId) {
        this.dealId = dealId;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusNext() {
        return this.statusNext;
    }

    public void setStatusNext(String statusNext) {
        this.statusNext = statusNext;
    }

    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public java.sql.Timestamp getInsertTimestamp() {
        return this.insertTimestamp;
    }

    public void setInsertTimestamp(java.sql.Timestamp insertTimestamp) {
        this.insertTimestamp = insertTimestamp;
    }
}

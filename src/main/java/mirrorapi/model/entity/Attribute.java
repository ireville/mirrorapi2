package mirrorapi.model.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;

@Entity
@Table(name = "\"DEALS_ATTRIBUTES\"")
public class Attribute {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "\"Id\"")
    private Long id;

    @Column(name = "\"DealId\"")
    private Long dealId;

    @Column(name = "\"Key\"", length = 32)
    private String key;

    @Column(name = "\"Value\"", length = 32)
    private String value;

    @Column(name = "\"DataUpdate\"", length = 2)
    private String dataUpdate;

    @Column(name = "\"InsertTimestamp\"")
    @CreationTimestamp
    private java.sql.Timestamp insertTimestamp;

    public Attribute(Long dealId, String dataUpdate){
        this.dealId = dealId;
        this.dataUpdate = (dataUpdate.equals("WA") ? "A" : (dataUpdate.equals("WU") ? "U" : ""));
    }

    public Attribute(String key, String value){
        this.key = key;
        this.value = value;
    }

    public Attribute() { }

    @Override
    public String toString(){
        return "\"" + key + "\":\"" + value + "\"";
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDealId() {
        return this.dealId;
    }

    public void setDealId(Long dealId) {
        this.dealId = dealId;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDataUpdate() {
        return this.dataUpdate;
    }

    public void setDataUpdate(String dataUpdate) {
        this.dataUpdate = dataUpdate;
    }

    public java.sql.Timestamp getInsertTimestamp() {
        return this.insertTimestamp;
    }

    public void setInsertTimestamp(java.sql.Timestamp insertTimestamp) {
        this.insertTimestamp = insertTimestamp;
    }
}

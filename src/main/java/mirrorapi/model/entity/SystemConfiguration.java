package mirrorapi.model.entity;

import javax.persistence.*;

@Entity
@Table(name = "\"SYSTEM_CONFIGURATION\"")
public class SystemConfiguration {
    @Id
    @Column(name = "\"Key\"")
    private String key;

    @Column(name = "\"Value\"")
    private String value;

    @Column(name = "\"ValueNew\"")
    private String valueNew;

    @Column(name = "\"ActionId\"")
    private Long actionId;


    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValueNew() {
        return this.valueNew;
    }

    public void setValueNew(String valueNew) {
        this.valueNew = valueNew;
    }

    public Long getActionId() {
        return this.actionId;
    }

    public void setActionId(Long actionId) {
        this.actionId = actionId;
    }
}

package mirrorapi.model.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;

@Entity
@Table(name = "\"DEALS_PARTIES\"")
public class Party {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "\"Id\"")
    private Long id;

    @Column(name = "\"DealId\"")
    private Long dealId;

    @Column(name = "\"PartyId\"", length = 32)
    private String partyId;

    @Column(name = "\"Role\"", length = 32)
    private String role;

    @Column(name = "\"DataUpdate\"", length = 2)
    private String dataUpdate;

    @Column(name = "\"InsertTimestamp\"")
    @CreationTimestamp
    private java.sql.Timestamp insertTimestamp;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDealId() {
        return this.dealId;
    }

    public void setDealId(Long dealId) {
        this.dealId = dealId;
    }

    public String getPartyId() {
        return this.partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDataUpdate() {
        return this.dataUpdate;
    }

    public void setDataUpdate(String dataUpdate) {
        this.dataUpdate = dataUpdate;
    }

    public java.sql.Timestamp getInsertTimestamp() {
        return this.insertTimestamp;
    }

    public void setInsertTimestamp(java.sql.Timestamp insertTimestamp) {
        this.insertTimestamp = insertTimestamp;
    }
}

package mirrorapi.model.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;

@Entity
@Table(name = "\"DEALS\"")
public class Deal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "\"Id\"")
    private Long id;

    @Column(name = "\"Kind\"", length = 32)
    private String kind;

    @Column(name = "\"Status\"", length = 32)
    private String status;

    @Column(name = "\"BlockChainId\"", length = 100)
    private String blockChainId;

    @Column(name = "\"Parent\"")
    private Long parent;

    @Column(name = "\"ParentBlockChainId\"", length = 100)
    private String parentBlockChainId;

    @Column(name = "\"R_Address\"", length = 256)
    private String rAddress;

    @Column(name = "\"Version\"", length = 100)
    private String version;

    @Column(name = "\"Data\"", length = 16000)
    private String data;

    @Column(name = "\"DataFile\"", length = 100)
    private String dataFile;

    @Column(name = "\"DataUpdate\"", length = 2)
    private String dataUpdate;

    @Column(name = "\"ActionId\"")
    private Long actionId;

    @Column(name = "\"Remark\"", length = 1000)
    private String remark;

    @Column(name = "\"DealsUUID\"", length = 64)
    private String dealsUuid;

    @Column(name = "\"Locked\"", length = 2)
    private String locked;

    @Column(name = "\"ArbitrationKind\"", length = 32)
    private String arbitrationKind;

    @Column(name = "\"ArbitrationBlockChainId\"", length = 100)
    private String arbitrationBlockChainId;

    @Column(name = "\"OracleNextTime\"")
    private Long oracleNextTime;

    @Column(name = "\"OracleData\"", length = 256)
    private String oracleData;

    @Column(name = "\"OracleVersion\"", length = 100)
    private String oracleVersion;

    @Column(name = "\"TxnBlock\"", length = 32)
    private String txnBlock;

    @Column(name = "\"InsertTimestamp\"")
    @CreationTimestamp
    private java.sql.Timestamp insertTimestamp;

    @Override
    public String toString(){
        ObjectMapper mapper = new ObjectMapper();

        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    public Deal(String kind, String data, Long parent, String dataUpdate, String dealsUuid) {
        this.kind = kind;
        this.data = data;
        this.parent = parent;
        this.dataUpdate = dataUpdate;
        this.dealsUuid = dealsUuid;
    }

    public Deal(String status, String remark, String dataUpdate) {
        this.status = status;
        this.remark = remark;
        this.dataUpdate = dataUpdate;
    }

    public Deal() {}

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKind() {
        return this.kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBlockChainId() {
        return this.blockChainId;
    }

    public void setBlockChainId(String blockChainId) {
        this.blockChainId = blockChainId;
    }

    public Long getParent() {
        return this.parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    public String getParentBlockChainId() {
        return this.parentBlockChainId;
    }

    public void setParentBlockChainId(String parentBlockChainId) {
        this.parentBlockChainId = parentBlockChainId;
    }

    public String getRAddress() {
        return this.rAddress;
    }

    public void setRAddress(String rAddress) {
        this.rAddress = rAddress;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getData() {
        return this.data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDataFile() {
        return this.dataFile;
    }

    public void setDataFile(String dataFile) {
        this.dataFile = dataFile;
    }

    public String getDataUpdate() {
        return this.dataUpdate;
    }

    public void setDataUpdate(String dataUpdate) {
        this.dataUpdate = dataUpdate;
    }

    public Long getActionId() {
        return this.actionId;
    }

    public void setActionId(Long actionId) {
        this.actionId = actionId;
    }

    public String getRemark() {
        return this.remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDealsUuid() {
        return this.dealsUuid;
    }

    public void setDealsUuid(String dealsUuid) {
        this.dealsUuid = dealsUuid;
    }

    public String getLocked() {
        return this.locked;
    }

    public void setLocked(String locked) {
        this.locked = locked;
    }

    public String getArbitrationKind() {
        return this.arbitrationKind;
    }

    public void setArbitrationKind(String arbitrationKind) {
        this.arbitrationKind = arbitrationKind;
    }

    public String getArbitrationBlockChainId() {
        return this.arbitrationBlockChainId;
    }

    public void setArbitrationBlockChainId(String arbitrationBlockChainId) {
        this.arbitrationBlockChainId = arbitrationBlockChainId;
    }

    public Long getOracleNextTime() {
        return this.oracleNextTime;
    }

    public void setOracleNextTime(Long oracleNextTime) {
        this.oracleNextTime = oracleNextTime;
    }

    public String getOracleData() {
        return this.oracleData;
    }

    public void setOracleData(String oracleData) {
        this.oracleData = oracleData;
    }

    public String getOracleVersion() {
        return this.oracleVersion;
    }

    public void setOracleVersion(String oracleVersion) {
        this.oracleVersion = oracleVersion;
    }

    public String getTxnBlock() {
        return this.txnBlock;
    }

    public void setTxnBlock(String txnBlock) {
        this.txnBlock = txnBlock;
    }

    public java.sql.Timestamp getInsertTimestamp() {
        return this.insertTimestamp;
    }

    public void setInsertTimestamp(java.sql.Timestamp insertTimestamp) {
        this.insertTimestamp = insertTimestamp;
    }
}

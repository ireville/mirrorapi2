package mirrorapi.model.response;

import mirrorapi.model.entity.Deal;

public class DealState {
    private final Long id;
    private final String dealsUuid;
    private final String blockChainId;
    private final String kind;
    private final String version;
    private final String status;

    public DealState(Deal deal) {
        this.id = deal.getId();
        this.dealsUuid = deal.getDealsUuid();
        this.blockChainId = deal.getBlockChainId();
        this.kind = deal.getKind();
        this.version = deal.getVersion();
        this.status = deal.getStatus();
    }

    public Long getId() {
        return id;
    }

    public String getDealsUuid() {
        return dealsUuid;
    }

    public String getBlockChainId() {
        return blockChainId;
    }

    public String getKind() {
        return kind;
    }

    public String getVersion() {
        return version;
    }

    public String getStatus() {
        return status;
    }
}

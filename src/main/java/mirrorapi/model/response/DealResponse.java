package mirrorapi.model.response;

import mirrorapi.model.entity.*;

import java.util.List;

public class DealResponse {
    private final Deal deal;
    private final List<Attribute> dealAttributes;
    private final List<Party> dealParties;
    private final List<File> dealFiles;
    private final List<StatusMap> dealStatusMaps;
    private final List<History> dealHistory;

    public DealResponse(Deal deal, List<Attribute> dealAttributes, List<Party> dealParties, List<File> dealFiles,
                        List<StatusMap> dealStatusMaps, List<History> dealHistory) {
        this.deal = deal;
        this.dealAttributes = dealAttributes;
        this.dealParties = dealParties;
        this.dealFiles = dealFiles;
        this.dealStatusMaps = dealStatusMaps;
        this.dealHistory = dealHistory;
    }

    public Deal getDeal() {
        return deal;
    }

    public List<Attribute> getDealAttributes() {
        return dealAttributes;
    }
    public List<Party> getDealParties() {
        return dealParties;
    }

    public List<File> getDealFiles() {
        return dealFiles;
    }

    public List<History> getDealHistory() {
        return dealHistory;
    }
    public List<StatusMap> getDealStatusMaps() {
        return dealStatusMaps;
    }
}

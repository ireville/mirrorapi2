package mirrorapi.model.request;

import mirrorapi.model.entity.Attribute;
import mirrorapi.model.entity.Party;
import mirrorapi.model.entity.StatusMap;

import java.util.List;

public class CreateDealRequest {
    private final String kind;
    private final String data;
    private final String dealsUuid;
    private final Long parent;
    private final List<Attribute> attributes;
    private final List<Party> parties;
    private final List<StatusMap> statusMaps;

    public CreateDealRequest(String kind, String data, String dealsUuid, Long parent, List<Attribute> attributes,
                             List<Party> parties, List<StatusMap> statusMaps) {
        this.kind = kind;
        this.data = data;
        this.dealsUuid = dealsUuid;
        this.parent = parent;
        this.attributes = attributes;
        this.parties = parties;
        this.statusMaps = statusMaps;
    }

    public String getKind() {
        return kind;
    }

    public String getData() {
        return data;
    }

    public String getDealsUuid() {
        return dealsUuid;
    }

    public Long getParent() {
        return parent;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public List<Party> getParties() {
        return parties;
    }

    public List<StatusMap> getStatusMaps() {
        return statusMaps;
    }
}

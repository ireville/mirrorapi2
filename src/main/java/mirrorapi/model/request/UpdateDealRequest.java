package mirrorapi.model.request;

import mirrorapi.model.entity.Attribute;
import mirrorapi.model.entity.File;

import java.util.List;

public class UpdateDealRequest {
    private final String kind;
    private final String status;
    private final String remark;
    private final boolean dataReplication;
    private final List<Attribute> dealAttributes;
    private final List<File> files;

    public UpdateDealRequest(String kind, String status, String remark, boolean dataReplication,
                             List<Attribute> dealAttributes, List<File> files) {
        this.kind = kind;
        this.status = status;
        this.remark = remark;
        this.dataReplication = dataReplication;
        this.dealAttributes = dealAttributes;
        this.files = files;
    }

    public String getKind() {
        return kind;
    }

    public String getStatus() {
        return status;
    }

    public String getRemark() {
        return remark;
    }

    public boolean isDataReplication() {
        return dataReplication;
    }

    public List<Attribute> getDealAttributes() {
        return dealAttributes;
    }

    public List<File> getFiles() {
        return files;
    }
}

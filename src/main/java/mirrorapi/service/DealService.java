package mirrorapi.service;

import mirrorapi.exception.ResourceNotFoundException;
import mirrorapi.model.entity.*;
import mirrorapi.model.request.CreateDealRequest;
import mirrorapi.model.request.UpdateDealRequest;
import mirrorapi.model.response.DealResponse;
import mirrorapi.model.response.DealState;
import mirrorapi.repository.*;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class DealService {

    private final DealRepository dealRepository;

    private final PartiesRepository partiesRepository;

    private final StatusMapRepository statusmapRepository;

    private final ActionRepository actionRepository;

    private final AttributeRepository attributeRepository;

    private final HistoryRepository historyRepository;

    private final FileRepository fileRepository;

    private final SystemConfigurationRepository systemConfigurationRepository;

    public DealService(DealRepository dealRepository, PartiesRepository partiesRepository,
                       StatusMapRepository statusmapRepository, ActionRepository actionRepository,
                       AttributeRepository attributeRepository, HistoryRepository historyRepository,
                       FileRepository fileRepository, SystemConfigurationRepository systemConfigurationRepository) {
        this.dealRepository = dealRepository;
        this.partiesRepository = partiesRepository;
        this.statusmapRepository = statusmapRepository;
        this.actionRepository = actionRepository;
        this.attributeRepository = attributeRepository;
        this.historyRepository = historyRepository;
        this.fileRepository = fileRepository;
        this.systemConfigurationRepository = systemConfigurationRepository;
    }

    /**
     * Gets all deals.
     * @return Short information about all deals.
     */
    public List<DealState> getAll() {
        List<Deal> all = dealRepository.findAll();
        return all.stream().map(DealState::new).collect(Collectors.toList());
    }

    /**
     * Receives a short tech deal information by its unique identifier.
     * @param id Deal identifier.
     * @return Response with short tech information about deal.
     */
    public String getByIdTech(Long id) {
        Deal foundDeal = dealRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Deal not found with id " + id));
        return foundDeal.getBlockChainId();
    }

    /**
     * Creates a deal.
     * @param request Information about the parameters of the created deal.
     * @return Unique identifier assigned to the deal.
     */
    public Long create(CreateDealRequest request) {
        // Проверка на наличие kind в соотв. таблице
        if (!systemConfigurationRepository.existsByKey(request.getKind() + " Template")) {
            return -1L;
        }

        // Создаём сделку и помечаем DataUpdate как "WA".
        Deal deal = new Deal(request.getKind(), request.getData(), request.getParent(), "WA",
                request.getDealsUuid());
        Deal addedDeal = dealRepository.save(deal);


        if (request.getParties() != null) {
            // Связываем переданных участников с созданной сделкой.
            request.getParties().forEach(party -> {
                party.setDealId(addedDeal.getId());
                party.setDataUpdate("A");
            });

            // Данные по участникам заносятся в таблицу DEALS_PARTIES.
            partiesRepository.saveAll(request.getParties());
        }

        // В таблицу DEALS_STATUSMAP заносится карта допустимых переходов состояний сделки.
        if (request.getStatusMaps() != null) {
            // Связываем переданные карты с созданной сделкой.
            request.getStatusMaps().forEach(statusMap -> statusMap.setDealId(addedDeal.getId()));

            statusmapRepository.saveAll(request.getStatusMaps());
        }


        if (request.getAttributes() != null) {
            // Связываем переданные атрибуты с созданной сделкой.
            request.getAttributes().forEach(attribute -> {
                attribute.setDealId(addedDeal.getId());
                attribute.setDataUpdate("A");
            });

            // В таблицу DEALS_ATTRIBUTES заносим все атрибуты сделки.
            attributeRepository.saveAll(request.getAttributes());
        }

        // В таблицу DEALS_ACTIONS заносится операция {Action=’AddDeal’, Executor=…, Data=<>, Status=’NEW’}.
        actionRepository.save(createAction(addedDeal, request.getAttributes()));

        attributeRepository.save(new Attribute(addedDeal.getId(), addedDeal.getDataUpdate()));

        historyRepository.save(new History(addedDeal));

        // Меняем статус сделки на "Создана".
        dealRepository.findById(addedDeal.getId())
                .map(newDeal -> {
                    newDeal.setDataUpdate("A");
                    return dealRepository.save(newDeal);
                }).orElseThrow(() -> new ResourceNotFoundException("Deal not found with id " + addedDeal.getId()));

        return addedDeal.getId();
    }

    /**
     * Updates a deal by its unique identifier.
     * @param id Deal identifier.
     * @param request Information about the new parameters for the deal.
     * @return Unique identifier previously assigned to the updated deal.
     */
    public Long update(final Long id, final UpdateDealRequest request) {
        // Меняем статус на "В процессе обновления".
        Deal updatedDeal = dealRepository.findById(id)
                .map(deal -> {
                    deal.setDataUpdate("WU");
                    return dealRepository.save(deal);
                }).orElseThrow(() -> new ResourceNotFoundException("Deal not found with id " + id));


        if (request.getFiles() != null) {
            // Заполняем поле Status  переданных файлов.
            request.getFiles().forEach(file -> {
                file.setDealId(updatedDeal.getId());
                file.setKind(request.getKind());
                file.setStatus(request.getStatus());
                file.setRemark(request.getRemark());
                file.setDataUpdate("A");
            });

            // В таблицу DEALS_FILES заносится перечень файлов, прилагаемых к сделке в новом статусе.
            fileRepository.saveAll(request.getFiles());
        }

        // Обновляем атрибуты.
        if (request.getDealAttributes() != null) {
            request.getDealAttributes().forEach(attribute -> {
                attribute.setDealId(updatedDeal.getId());
                attribute.setDataUpdate("U");
            });
        }

        // В таблицу DEALS_ACTIONS заносится операция {Action=’SetStatus’, Executor=…, Data=<>, Status=’NEW’}
        actionRepository.save(updateAction(request, updatedDeal, request.getDealAttributes()));

        // Меняем статус сделки на "Обновлена".
        dealRepository.findById(id)
                .map(deal -> {
                    deal.setDataUpdate("U");
                    return dealRepository.save(deal);
                }).orElseThrow(() -> new ResourceNotFoundException("Deal not found with id " + id));

        return updatedDeal.getId();
    }

    /**
     * Receives a full deal information by its unique identifier.
     * @param id Deal identifier.
     * @return Response with full information about deal and related materials.
     */
    public DealResponse getById(Long id) {

        Deal foundDeal = dealRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Deal not found with id " + id));

        List<Attribute> foundDealAttributes = attributeRepository.findByDealId(id);
        List<Party> foundDealParties = partiesRepository.findByDealId(id);
        List<StatusMap> foundDealStatusMaps = statusmapRepository.findByDealId(id);
        List<File> foundDealFiles = fileRepository.findByDealId(id);
        List<History> foundDealHistory = historyRepository.findByDealId(id);

        return new DealResponse(foundDeal, foundDealAttributes, foundDealParties, foundDealFiles, foundDealStatusMaps,
                foundDealHistory);
    }

    /**
     * Deletes deal by its unique.
     * @param id Deal identifier.
     * @return HttpResponse.
     */
    public ResponseEntity<?> delete(Long id) {
        return dealRepository.findById(id)
                .map(deal -> {
                    dealRepository.delete(deal);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Deal not found with id " + id));
    }


    /**
     * Creates an Action for the deal creation.
     * @param deal Created deal.
     * @param attributes Attributes of the created deal.
     * @return Created Action.
     */
    private Action createAction(Deal deal, List<Attribute> attributes) {
        return new Action("AddDeal", "", createActionData(deal, attributes), "NEW");
    }

    /**
     * Creates Data field for the Action for the deal creation.
     * @param addedDeal Created deal.
     * @param attributes Attributes of the created deal.
     * @return Action's Data field.
     */
    private String createActionData(Deal addedDeal, List<Attribute> attributes) {
        List<String> stringAttributes = new ArrayList<>();
        stringAttributes.add("\"Id\":\"" + addedDeal.getId() + "\"");

        if(attributes != null) {
            stringAttributes.addAll(attributes.stream().map(Object::toString).collect(Collectors.toList()));
        }

        return "{" + String.join(",", stringAttributes) + "}";
    }

    /**
     * Creates an Action for the deal update.
     * @param request Request for the deal update.
     * @param updatedDeal Updated deal.
     * @param attributes Attributes of the updated deal.
     * @return Created Action.
     */
    private Action updateAction(UpdateDealRequest request, Deal updatedDeal,
                                List<Attribute> attributes) {
        return new Action("SetStatus", "", updateActionData(request, updatedDeal, attributes), "NEW");
    }

    /**
     * Creates Data field for the Action for the deal update.
     * @param request Request for the deal update.
     * @param updatedDeal Updated deal.
     * @param attributes Attributes of the updated deal.
     * @return Action's Data field.
     */
    private String updateActionData(UpdateDealRequest request, Deal updatedDeal, List<Attribute> attributes) {
        List<String> stringAttributes = new ArrayList<>();
        stringAttributes.add("\"Id\":\"" + updatedDeal.getId() + "\"");
        stringAttributes.add("\"Status\":\"" + request.getStatus() + "\"");
        stringAttributes.add("\"Remark\":\"" + request.getRemark() + "\"");
        stringAttributes.add("\"Data\":\"" + (request.isDataReplication() ? "Y" : "N") + "\"");

        if(request.getFiles() != null && !request.getFiles().isEmpty()) {
            stringAttributes.add("\"Files\":\"" +
                    request.getFiles().stream().map(File::getFileUuid).collect(Collectors.joining(",")) + "\"");
        }

        if (attributes != null && !attributes.isEmpty()) {
            stringAttributes.addAll(attributes.stream().map(Object::toString).collect(Collectors.toList()));
        }

        return "{" + String.join(",", stringAttributes) + "}";
    }
}

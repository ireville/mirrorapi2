package mirrorapi.controller;

import io.swagger.annotations.ApiOperation;
import mirrorapi.model.request.CreateDealRequest;
import mirrorapi.model.request.UpdateDealRequest;
import mirrorapi.model.response.DealResponse;
import mirrorapi.model.response.DealState;
import mirrorapi.repository.*;
import mirrorapi.service.DealService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Controller that handles deal requests.
 */
@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/deals")
public class DealController {

    private final DealRepository dealRepository;

    private final DealService dealService;

    public DealController(DealRepository dealRepository, DealService dealService) {
        this.dealRepository = dealRepository;
        this.dealService = dealService;
    }

    /**
     *  Request handler to get all deals.
     * @return Short information about all deals.
     */
    @GetMapping()
    @ApiOperation("Get All Deals")
    public List<DealState> getAll() {
        return dealService.getAll();
    }

    /**
     * Handler of a request to receive a full deal information by its unique identifier.
     * @param id Deal identifier.
     * @return Response with full information about deal and related materials.
     */
    @GetMapping("/{id}")
    @ApiOperation("Get Full Deal Information")
    public DealResponse getById(@PathVariable("id") Long id) {
        return dealService.getById(id);
    }

    /**
     * Handler of a request to receive a short tech deal information by its unique identifier.
     * @param id Deal identifier.
     * @return Response with short tech information about deal.
     */
    @GetMapping("/{id}/tech")
    @ApiOperation("Get Short Tech Deal Information")
    public String getByIdTech(@PathVariable("id") Long id) {
        return dealService.getByIdTech(id);
    }

    /**
     * Deal creation request handler.
     * @param request Information about the parameters of the created deal.
     * @return Unique identifier assigned to the deal.
     */
    @PostMapping()
    @ApiOperation("Create a Deal")
    public Long create(@Valid @RequestBody CreateDealRequest request) {
        return dealService.create(request);
    }

    /**
     * Deal update by its unique identifier request handler.
     * @param id Deal identifier.
     * @param request Information about the new parameters for the deal.
     * @return Unique identifier previously assigned to the updated deal.
     */
    @PutMapping("/{id}")
    @ApiOperation("Update a Deal")
    public Long update(@PathVariable final Long id,
                         @Valid @RequestBody final UpdateDealRequest request) {
        return dealService.update(id, request);
    }

    /**
     * Delete deal by its unique identifier request handler.
     * @param id Deal identifier.
     * @return HttpResponse.
     */
    @DeleteMapping("/{id}")
    @ApiOperation("Delete a Deal")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        return dealService.delete(id);
    }

    /**
     * Delete all deal request handler.
     */
    @DeleteMapping()
    @ApiOperation("Delete All Deals")
    public void delete() {
        dealRepository.deleteAll();
    }
}


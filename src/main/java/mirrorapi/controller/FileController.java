package mirrorapi.controller;

import io.swagger.annotations.ApiOperation;
import mirrorapi.model.entity.File;
import mirrorapi.repository.FileRepository;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/files")
public class FileController {
    private final FileRepository fileRepository;

    public FileController(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    /**
     * Handler of a request to receive file's information by file identifier.
     * @param uuid File identifier.
     * @return File's information.
     */
    @GetMapping("/{uuid}")
    @ApiOperation("Get File By It's UUID")
    public List<File> getByUUId(@PathVariable("uuid") String uuid) {
        return fileRepository.findByFileUuid(uuid);
    }

    /**
     * Handler of a request to receive information about all files related to a deal by deal identifier.
     * @param id Deal identifier.
     * @return File's information.
     */
    @GetMapping("/fromDeal/{id}")
    @ApiOperation("Get Files Related To The Deal With Id")
    public List<File> getById(@PathVariable("id") Long id) {
        return fileRepository.findByDealId(id);
    }


    /**
     * Request handler for adding information about a file
     * @param file File added.
     * @return Added file with updated information.
     */
    @PostMapping()
    @ApiOperation("Add a file")
    public File create(@Valid @RequestBody File file) {
        return fileRepository.save(file);
    }
}
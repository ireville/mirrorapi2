package mirrorapi.repository;

import mirrorapi.model.entity.SystemConfiguration;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SystemConfigurationRepository extends JpaRepository<SystemConfiguration, Long> {
    boolean existsByKey(String key);
}

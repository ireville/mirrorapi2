package mirrorapi.repository;

import mirrorapi.model.entity.Attribute;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AttributeRepository extends JpaRepository<Attribute, Long> {
    List<Attribute> findByDealId(Long dealId);
}

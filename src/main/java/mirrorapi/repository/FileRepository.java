package mirrorapi.repository;

import mirrorapi.model.entity.File;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FileRepository extends JpaRepository<File, Long> {
    List<File> findByDealId(Long dealId);
    List<File> findByFileUuid(String fileUuid);
}

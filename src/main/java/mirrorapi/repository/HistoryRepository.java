package mirrorapi.repository;

import mirrorapi.model.entity.History;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HistoryRepository extends JpaRepository<History, Long> {
    List<History> findByDealId(Long dealId);
}

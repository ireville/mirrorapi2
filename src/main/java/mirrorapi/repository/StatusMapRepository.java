package mirrorapi.repository;

import mirrorapi.model.entity.StatusMap;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StatusMapRepository extends JpaRepository<StatusMap, Long> {
    List<StatusMap> findByDealId(Long dealId);
}

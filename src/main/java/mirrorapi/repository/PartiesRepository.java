package mirrorapi.repository;

import mirrorapi.model.entity.Party;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PartiesRepository extends JpaRepository<Party, Long> {
    List<Party> findByDealId(Long dealId);
}

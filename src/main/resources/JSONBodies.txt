CreateDeal json body:
   {
       "kind": "Order",
       "data": null,
       "dealsUuid": "dealsUuid1",
       "parent": 1234,
       "dealAttributes": [
           {
               "id": null,
               "dealId": null,
               "key": "key1",
               "value": "value1",
               "dataUpdate": null,
               "insertTimestamp": null
           },
           {
               "id": null,
               "dealId": null,
               "key": "key2",
               "value": "value2",
               "dataUpdate": null,
               "insertTimestamp": null
           }
       ],
       "parties": null,
       "statusMaps": null
   }



UpdateDealRequest json body:
    {
        "status": "updateStatus",
        "remark": "updateRemark",
        "dataReplication": false,
        "dealAttributes": [
            {
                "id": null,
                "dealId": null,
                "key": "key3",
                "value": "value3",
                "dataUpdate": null,
                "insertTimestamp": null
            },
            {
                "id": null,
                "dealId": null,
                "key": "key4",
                "value": "value4",
                "dataUpdate": null,
                "insertTimestamp": null
            }
        ],
        "files": [
            {
                "id": null,
                "dealId": null,
                "parent": null,
                "relation": null,
                "kind": "kindFile1",
                "status": "statusFile1",
                "localPath": null,
                "dfsPath": null,
                "hash": null,
                "sign": null,
                "error": null,
                "dataUpdate": null,
                "version": null,
                "remark": "remarkFile1",
                "recipients": null,
                "fileUuid": "fileUuid1",
                "insertTimestamp": null,
                "fromSc": null
            },
            {
                "id": null,
                "dealId": null,
                "parent": null,
                "relation": null,
                "kind": "kindFile2",
                "status": "statusFile2",
                "localPath": null,
                "dfsPath": null,
                "hash": null,
                "sign": null,
                "error": null,
                "dataUpdate": null,
                "version": null,
                "remark": "remarkFile2",
                "recipients": null,
                "fileUuid": "fileUuid2",
                "insertTimestamp": null,
                "fromSc": null
            }
        ]
    }
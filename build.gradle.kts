plugins {
    java
    application
    idea
    id("org.springframework.boot") version "2.4.4"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("org.postgresql:postgresql:42.2.19")

    implementation("io.springfox:springfox-boot-starter:3.0.0")

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.7.1")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
}

application {
    applicationName = "MirrorAPI"
    description = "MirrorAPI"
    mainClass.set("mirrorapi.MirrorApiApplication")
}

java {
    targetCompatibility = JavaVersion.VERSION_1_8
    sourceCompatibility = JavaVersion.VERSION_1_8
}

tasks {
    withType<JavaCompile> {
        options.encoding = "UTF-8"
    }

    jar {
        manifest {
            attributes["Main-Class"] = project.application.mainClass
        }
        archiveBaseName.set("MirrorAPI")
        archiveVersion.set("")
    }

    distZip {
        archiveBaseName.set(jar.get().archiveBaseName)
        archiveVersion.set(jar.get().archiveVersion)
        archiveAppendix.set("dist")
    }

    distTar {
        isEnabled = false
    }

    withType<Test> {
        useJUnitPlatform()
        systemProperty("file.encoding", "UTF-8")
    }
}